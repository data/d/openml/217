# OpenML dataset: pyrim

https://www.openml.org/d/217

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown -   
**Please cite**:   

The task consists of Learning Quantitative Structure Activity
 Relationships (QSARs). The Inhibition of Dihydrofolate Reductase by
 Pyrimidines.The data are described in: King, Ross .D., Muggleton,
 Steven., Lewis, Richard. and Sternberg, Michael.J.E. Drug Design by
 machine learning: the use of inductive logic programming to model the
 structure-activity relationships of trimethoprim analogues binding to
 dihydrofolate reductase.
 
 Original source: ?. 
 Source: collection of regression datasets by Luis Torgo (ltorgo@ncc.up.pt) at
 http://www.ncc.up.pt/~ltorgo/Regression/DataSets.html
 Characteristics: 74 cases; 28 continuous variables

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/217) of an [OpenML dataset](https://www.openml.org/d/217). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/217/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/217/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/217/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

